﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenChatBotExperiment
{
    class Message
    {
        private DateTime date;
        private String name;
        private String nickname;
        private String timeTooltip;
        private int line;
        private String content;
        private IEnumerable<String> emojiUrls;

        public Message(DateTime date, String name, String nickname, String timeTooltip, int line, String content, IEnumerable<String> emojiUrls)
        {
            this.date = date;
            this.name = name;
            this.nickname = String.IsNullOrEmpty(nickname) ? name.Substring(0, name.IndexOf(' ')) : nickname;
            this.timeTooltip = timeTooltip;
            this.line = line;
            this.content = content;
            this.emojiUrls = emojiUrls;
        }

        public DateTime Date
        {
            get { return date; }
        }

        public String Name
        {
            get { return name; }
        }

        public String Nickname
        {
            get { return nickname; }
        }

        public String TimeTooltip
        {
            get { return timeTooltip; }
        }

        public int Line
        {
            get { return line; }
        }

        public String Content
        {
            get { return content; }
        }

        public IEnumerable<String> EmojiUrls
        {
            get { return emojiUrls; }
        }

        public override int GetHashCode()
        {
            String concat = String.Concat(date.Ticks, name, nickname, timeTooltip, line, content);
            return concat.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            Message other = obj as Message;
            if (other == null)
            {
                return false;
            }

            return
                other.date == Date &&
                other.Name == Name &&
                other.Nickname == Nickname &&
                other.TimeTooltip == TimeTooltip &&
                other.Line == Line &&
                other.Content == content;
        }
    }
}
