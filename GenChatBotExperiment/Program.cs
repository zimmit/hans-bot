﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Threading;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Chrome;
using Newtonsoft.Json.Linq;

namespace GenChatBotExperiment
{
    class Program
    {
        // This will probably stay the same, so no need to worry about auto-detect unless the whole messenger platform updates.
        private const String MESSAGES_DIV_SELECTOR = "[aria-label=Messages]";

        // TODO: We can and should probably determine this label in code.
        private const String MESSAGE_DIV_SELECTOR = "._1t_p.clearfix";

        private static object pollingMutext = new object();

        private static IWebDriver driver;
        private static Timer pollingTimer;
        private static Timer responseTimer;

        private static int offset;
        private static HashSet<Message> messages;
        private static ConcurrentQueue<String> responseQueue;

        // Dictionaries for images
        private static Dictionary<String, List<String>> dictionaries;
        private static Dictionary<String, String> synonyms;
        private static Dictionary<String, String> restrictions;
        private static Dictionary<String, DateTime> partyTimestamps;
        private static HashSet<String> blacklist;

        // test;

        static void Main(string[] args)
        {
            dictionaries = new Dictionary<String, List<String>>();
            synonyms = new Dictionary<String, String>();
            restrictions = new Dictionary<String, String>();
            partyTimestamps = new Dictionary<String, DateTime>();
            blacklist = new HashSet<String>();

            ChromeOptions options = new ChromeOptions();
            // options.AddArgument("--headless");
            // options.AddArgument("--disable-gpu");
            // options.AddArgument("remote-debugging-port=9222");

            driver = new ChromeDriver(options);
            driver.Navigate().GoToUrl("http://www.messenger.com");
            Console.WriteLine("DONE");

            messages = new HashSet<Message>();
            responseQueue = new ConcurrentQueue<String>();

            pollingTimer = new Timer(Poll, null, Timeout.Infinite, Timeout.Infinite);
            responseTimer = new Timer(Respond, null, Timeout.Infinite, Timeout.Infinite);

            AddImgurCommand("cats", String.Empty, "cat");
            AddImgurCommand("corgi", String.Empty, "corgi");
            AddImgurCommand("beer", String.Empty, "beer");
            AddImgurCommand("bears", String.Empty, "bear");
            AddImgurCommand("tacos", String.Empty, "taco");
            AddImgurCommand("nachos", "Emily", "nacho");

            Console.ReadLine();
            Console.WriteLine("GOING");

            while (true)
            {
                GroupChat chat = new GroupChat(driver);
                foreach(string name in chat.ReadNames())
                {
                    Console.WriteLine(name);
                }

                Console.ReadLine();
            }

            // Load up the currently visible messages so that we can cache them and have an idea of what we've "seen".
            Poll("no");
            // Reset the response queue so we don't respond to old messages when we initialize.
            responseQueue = new ConcurrentQueue<String>();
            // Start the response polling loop.
            Respond(null);

            Console.ReadLine();
        }

        private static bool AddImgurCommand(String subreddit, String restriction, String keyword)
        {
            List<String> images = new List<String>();
            String imgurGalleryPath = String.Format("https://api.imgur.com/3/gallery/r/{0}/{1}/{2}/{3}", subreddit, "top", "0", "year");

            WebRequest request = WebRequest.Create(imgurGalleryPath);
            request.Headers.Add("Authorization", "Client-ID a0e9f9a20599a30");

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            StreamReader reader = new StreamReader(response.GetResponseStream());
            String rawData = reader.ReadToEnd();

            JObject json = JObject.Parse(rawData);
            foreach (JToken child in json.GetValue("data").Children())
            {
                Boolean notSafeForWork = child.Value<Boolean>("nsfw");
                if (!notSafeForWork)
                {
                    images.Add(child.Value<String>("link"));
                }
            }

            if (images.Count > 0)
            {
                dictionaries[keyword] = images;
                restrictions[keyword] = restriction;
                return true;
            }

            return false;
        }

        private static bool TryResolveKeyword(String keyword, out String resolvedKeyword)
        {
            String normalizedKeyword = keyword.ToLowerInvariant();
            if (dictionaries.ContainsKey(keyword))
            {
                resolvedKeyword = keyword;
                return true;
            }
            else if (synonyms.TryGetValue(keyword, out resolvedKeyword))
            {
                return true;
            }
            else
            {
                // Leave it the same for now because we're hacking shit together and this makes stuff work better.
                resolvedKeyword = keyword;
                return false;
            }
        }

        private static IEnumerable<String> ProcessMessage(Message message)
        {
            List<String> responses = new List<String>();

            Random random = new Random();

            String name = message.Name.Substring(0, message.Name.IndexOf(' '));
            String normalizedContent = new String(message.Content.ToLowerInvariant().Where(c => char.IsLetterOrDigit(c) || c == '@' || c == ' ').ToArray());

            if (normalizedContent.StartsWith("@hans "))
            {
                if (blacklist.Contains(name.ToLowerInvariant()))
                {
                    return responses;
                }

                if (normalizedContent == "@hans paint with me")
                {
                    if (name == "Noah" || name == "Shelby")
                    {
                        responses.Add("Ah, yes, a splash of " + ClickColor() + "!");
                    }
                    else
                    {
                        responses.Add("I'm deeply sorry, but you don't work in an art museum. I can't trust your judgement");
                    }
                }

                String[] command = normalizedContent.Split(' ');
                if (command[1] == "get" || command[1] == "fetch" || command[1] == "show")
                {
                    for (int i = 2; i < command.Length; ++i)
                    {
                        String keyword;
                        if (TryResolveKeyword(command[i], out keyword))
                        {
                            if (!restrictions.ContainsKey(keyword) || restrictions[keyword] == String.Empty || message.Name.Contains(restrictions[keyword]))
                            {
                                String prefix = "I got you, " + name + ": ";
                                String response = dictionaries[keyword][random.Next() % dictionaries[keyword].Count];
                                responses.Add(prefix + response);
                            }
                            else
                            {
                                responses.Add("Uh, sorry " + name + "...that's " + restrictions[keyword] + "'s thing.");
                            }
                        }
                    }
                }
                else if (command.Length > 3 && command[1] == "stock" && command[2] == "up" && command[3] == "on")
                {
                    String keyword;
                    if (command.Length < 5 || String.IsNullOrWhiteSpace(command[4]))
                    {
                        responses.Add("Don't be daft, " + name + ". There's nothing there. It's 2017; learn to use a keyboard.");
                    }
                    else if (TryResolveKeyword(command[4], out keyword))
                    {
                        responses.Add("Fear not, " + name + "! We've already got some fine " + keyword + " in stock!");
                    }
                    else
                    {
                        if (AddImgurCommand(keyword, String.Empty, keyword))
                        {
                            responses.Add("I have made a quick grocery run and stocked up on " + keyword + " for " + name + ". Enjoy!");
                        }
                        else
                        {
                            responses.Add("They seem to be fresh out of " + keyword + "; that is, there's no images in the imgur subreddit for that");
                        }
                    }
                }
                else if (command[1] == "unstock")
                {
                    String keyword;
                    if (command.Length < 2 || String.IsNullOrWhiteSpace(command[2]))
                    {
                        responses.Add("Never fear, " + name + ". I've removed all the...nothing...from the storeroom for you.");
                    }
                    else if (TryResolveKeyword(command[2], out keyword))
                    {
                        responses.Add("I have removed all the " + keyword + " from the larder. How unsightly!");

                        dictionaries.Remove(keyword);
                        foreach (String alias in synonyms.Where(kvp => kvp.Value == keyword).Select(kvp => kvp.Key).ToArray())
                        {
                            synonyms.Remove(alias);
                        }
                    }
                    else
                    {
                        responses.Add("Alas. We do not have any " + keyword + " in the cellar to remove! Perhaps that's for the better.");
                    }
                }
                else if (command.Length > 3 && command[1] == "alias" && command[3] == "as")
                {
                    String keyword;
                    if (String.IsNullOrWhiteSpace(command[2]) || String.IsNullOrWhiteSpace(command[4]))
                    {
                        responses.Add("Stop that. That's blank.");
                    }
                    else if (!TryResolveKeyword(command[2], out keyword))
                    {
                        responses.Add("I don't even know what a " + keyword + " is.");
                    }
                    else if (TryResolveKeyword(command[4], out keyword))
                    {
                        responses.Add("I already think I know what a " + keyword + " is!");
                    }
                    else
                    {
                        TryResolveKeyword(command[2], out keyword);
                        responses.Add("Duly noted! Now " + command[4] + " also means " + command[2]);
                        synonyms[command[4]] = keyword;
                    }
                }
                else if (command.Length > 2 && command[1] == "party" && command[2] == "time")
                {
                    /*if (name == "Noah" || !partyTimestamps.ContainsKey(name) || DateTime.UtcNow - partyTimestamps[name] > TimeSpan.FromMinutes(5))
                    {
                        ClickColor(10);
                        partyTimestamps[name] = DateTime.UtcNow;
                    }
                    else
                    {
                        TimeSpan diff = TimeSpan.FromMinutes(5) - (DateTime.UtcNow - partyTimestamps[name]);
                        responses.Add("Yo, " + name + ", you literally just threw a party. Calm the fuck down for a sec. " + diff.TotalSeconds + " seconds, to be exact.");
                    }*/
                }
                else if (command[1] == "ban")
                {
                    String bad = command.Length >= 3 ? command[2] : String.Empty;
                    if (String.IsNullOrWhiteSpace(bad))
                    {
                        responses.Add("Sigh. Okay. Yes, " + name + ". I've banned that empty space for you.");
                    }
                    else if (bad == "all")
                    {
                        responses.Add("no");
                    }
                    else if (blacklist.Contains(bad))
                    {
                        responses.Add(bad + " is already on the banlist. Thankfully.");
                    }
                    else if (bad == "noah")
                    {
                        responses.Add("Lol right nice try maybe i'll ban you instead " + name);
                        responses.Add("congrats u played urself");
                        blacklist.Add(name.ToLowerInvariant());
                    }
                    else
                    {
                        responses.Add("Banned " + bad + " as per the whims of " + name + ".");
                        blacklist.Add(bad);
                    }
                }
                else if (command[1] == "unban")
                {
                    String bad = command.Length >= 3 ? command[2] : String.Empty;
                    if (String.IsNullOrWhiteSpace(bad))
                    {
                        responses.Add("Sigh. Yes. I've unbanned that empty space for you.");
                    }
                    else if (blacklist.Contains(bad))
                    {
                        responses.Add(bad + " is gloriously unbanned! Hooray!");
                        blacklist.Remove(bad);
                    }
                    else if (bad == "all")
                    {
                        if (name == "Noah")
                        {
                            responses.Add("Everyone's unbanned.");
                            blacklist.Clear();
                        }
                        else
                        {
                            responses.Add("I'm sorry, " + name + ", but I'm afraid I can't do that.");
                        }
                    }
                    else
                    {
                        responses.Add(bad + " is not banned! Relax. Chill. Calm the fuck down.");
                    }
                }
                else if (command[1] == "help")
                {
                    responses.Add("Hello! I'm the horrifyingly disembodied spirit of a manservant possessing this chat client. Ask me to fetch you something! Like beers! Or bears!");
                    responses.Add("If I don't have what you're looking for, feel free to ask me to stock up.");
                    responses.Add("If someone is using me for gross things, or you just feel like pissing them off, feel free to ask me to ban them (by their full first name, please)");
                    responses.Add("When you feel like having mercy, just ask me to unban them (again, by their full first name, please!");
                }
            }
            else
            {
                Console.WriteLine("maybe lana");
                // DANGER ZONE.
                List<char> chars = new List<char>();
                foreach (char c in normalizedContent)
                {
                    if (chars.Count == 0 || chars.Last() != c)
                    {
                        chars.Add(c);
                    }
                }

                Console.WriteLine(new String(chars.ToArray()));

                if (new String(chars.ToArray()) == "lana" && normalizedContent.Length > 10)
                {
                    responses.Add("https://www.youtube.com/watch?v=yK0P1Bk8Cx4");
                }
            }

            return responses;
        }

        private static String ClickColor(int times = 1)
        {
            lock (pollingMutext)
            {
                IWebElement colorButton = driver.FindElement(By.CssSelector("[data-intl-translation=\"Change Color\"]"));
                String name = String.Empty;

                for (int i = 0; i < times; ++i)
                {
                    Actions actions = new Actions(driver);
                    actions.MoveToElement(colorButton);
                    actions.Click();
                    actions.Build().Perform();

                    List<IWebElement> options = driver.FindElements(By.CssSelector("._5dr3")).ToList();
                    Random random = new Random();
                    IWebElement color = options[random.Next() % options.Count];
                    name = color.GetAttribute("aria-label");
                    actions.MoveToElement(color);
                    actions.Click();
                    actions.Build().Perform();
                }

                return name;
            }
        }

        private static void Respond(object state)
        {
            String responseString = String.Empty;
            int delay = 250;
            if (responseQueue.TryDequeue(out responseString))
            {
                IWebElement textbox = driver.FindElement(By.CssSelector("[data-text=true]"));

                Actions actions = new Actions(driver);
                actions.MoveToElement(textbox);
                actions.Click();
                actions.SendKeys(responseString);
                actions.Build().Perform();

                // Let image previews resolve.
                if (responseString.Contains("imgur") || responseString.Contains("youtube"))
                {
                    Thread.Sleep(1000);
                    delay = 100;
                }

                actions = new Actions(driver);
                actions.SendKeys(Keys.Return);
                actions.Build().Perform();
            }
            responseTimer.Change(delay, Timeout.Infinite);
        }

        private static void Poll(object state)
        {
            IWebElement messagesElement = null;
            IList<Message> messages = new List<Message>();

            try
            {
                messagesElement = driver.FindElement(By.CssSelector(MESSAGES_DIV_SELECTOR));
            }
            catch (NoSuchElementException)
            {
                // This usually means we switched chats while polling. While this isn't a typical
                // use case, it's nice to be able to do this during debugging without a hard crash.
            }

            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(messagesElement.GetAttribute("innerHTML"));

            HtmlNode messagesRoot = document.DocumentNode.ChildNodes[2];

            IEnumerable<HtmlNode> messageGroups = messagesRoot
                .ChildNodes
                .Where(node => node.OriginalName == "div")
                .Select(node => node.FirstChild);

            foreach (HtmlNode group in messageGroups)
            {
                string name = string.Empty;
                string nickname = string.Empty;
                string time = string.Empty;

                if (group.ChildNodes.Count == 1)
                {
                    name = "Noah Zimmt";
                }
                else if (group.ChildNodes.Count == 2)
                {
                    HtmlNode header = group.FirstChild;
                    HtmlNode image = header.FirstChild;
                    while (image.OriginalName != "img")
                    {
                        image = image.FirstChild;
                    }
                    name = image.Attributes["alt"].Value;
                }
                else
                {
                    // Nickname changes, color changes, etc.
                    continue;
                }

                HtmlNode content = group.LastChild;
                HtmlNode label = content
                    .FirstChild // Header tag.
                    .FirstChild;
                nickname = label.InnerText;

                IEnumerable<HtmlNode> lines = content
                    .ChildNodes
                    .Where(node => node.OriginalName == "div")
                    .Select(node => node.FirstChild);

                foreach (HtmlNode line in lines)
                {
                    // Some content does not have a time tooltip.
                    string timeString = line.GetAttributeValue("data-tooltip-content", string.Empty);
                    string textString = line
                        .FirstChild // Div tag.
                        .FirstChild // Span tag.
                        .InnerText;
                    IEnumerable<string> emojis = line
                        .Descendants()
                        .Where(node => node.OriginalName == "img")
                        .Where(node => node.Attributes["src"].Value.StartsWith("https://static.xx.fbcdn.net/images/emoji.php/"))
                        .Select(node => node.Attributes["src"].Value);
                    Console.WriteLine(textString);
                    Console.WriteLine(emojis.Count());
                }
            }

            /*
            Console.Title = "MESSAGES POLLED: " + messageElements.Count();
            foreach (IWebElement messageElement in messageElements.Skip(offset - 1))
            {
                // Grab the divs that make up each message "block" - continuous messages by the same person.
                IEnumerable<IWebElement> divs = messageElement.FindElements(By.CssSelector(MESSAGE_DIV_SELECTOR + " > div"));

                String name = String.Empty;
                String nickname = String.Empty;
                String timeTooltip = String.Empty;

                if (divs.Count() == 1)
                {
                    // TODO: Technically, this should be whoever is running the bot. Low priority.
                    name = "Noah Zimmt";
                }
                else if (divs.Count() == 2)
                {
                    // Header element contains name data, profile urls, image, etc.
                    IWebElement headerElement = divs.First();
                    // Grab the profile picture so we can take the alt-text.
                    IWebElement pictureElement = headerElement.FindElement(By.TagName("img"));
                    // Profile picture alt-text is the person's real name.
                    name = pictureElement.GetAttribute("alt");
                }
                else
                {
                    throw new InvalidOperationException("Unexpected number of divs in the message.");
                }

                // Content element contains message, links, images, timestamps, reactions.
                IWebElement contentElement = divs.Last();

                try
                {
                    // First header has the nickname.
                    IWebElement nicknameElement = contentElement.FindElement(By.TagName("h5"));
                    nickname = nicknameElement.Text;

                    // Content div has a tooltip, which has the timestamp.
                    IWebElement tooltipElement = contentElement.FindElement(By.CssSelector("[data-tooltip-content]"));
                    timeTooltip = tooltipElement.GetAttribute("data-tooltip-content");
                }
                catch (Exception)
                {
                    // Some types of content that we don't handle might not have a timestamp (event invites, etc.)
                    // Swallow this for now.
                }

                // Get the parent class so we can select all top-level message line divs.
                String parentClass = String.Format(".{0}", contentElement.GetAttribute("class"));
                int lineNumber = 0;
                foreach (IWebElement line in contentElement.FindElements(By.CssSelector(parentClass + " > div")))
                {
                    // Text content is the second div down. Drop the +1s from reactions and read-by bubbles.
                    IWebElement lineText = line.FindElement(By.CssSelector("div > div")).FindElement(By.CssSelector("div > div"));

                    // Get all the images in the message.
                    IEnumerable<IWebElement> imageElements = line.FindElements(By.TagName("img"));

                    // Filter non-emojis by looking for the facebook emoji serving URL.
                    IEnumerable<IWebElement> emojiElements = imageElements.Where(image =>
                    {
                        try
                        {
                            return image.GetAttribute("src").StartsWith("https://static.xx.fbcdn.net/images/emoji.php/");
                        }
                        catch (StaleElementReferenceException)
                        {
                            // Some images will be removed from the DOM before we process them (usually the seen-by bubbles).
                            // We don't care about them, so it's okay to swallow this exception and return.
                            return false;
                        }
                    });

                    List<String> emojiUrls = emojiElements.Select(emoji => emoji.GetAttribute("src")).ToList();
                    Message message = new Message(DateTime.UtcNow.Date, name, nickname, timeTooltip, lineNumber, lineText.Text, emojiUrls);
                    lineNumber++;

                    if (messages.Add(message))
                    {
                        Console.WriteLine(JObject.FromObject(message));
                        Console.WriteLine(state);
                        // Do not process on the first run.
                        if (state == null)
                        {
                            IEnumerable<String> responses = ProcessMessage(message);
                            foreach (String response in responses)
                            {
                                responseQueue.Enqueue(response);
                            }
                        }
                    }
                }

            }

            offset += messageElements.Skip(offset).Count();

            // Not sure what a good value to set here is; want to prevent the browser from slowing down as it fills up with chats.
            // Also want to prevent us from having to retrieve a ton of divs every time we poll, since there's no upper bound on
            // the number of lines that can be within one message div (i.e., rants).
            if (offset > 50)
            {
                driver.Navigate().Refresh();
                offset = 0;
            }*/

            pollingTimer.Change(1000, Timeout.Infinite);
        }
    }
}
