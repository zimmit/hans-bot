﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using Newtonsoft.Json.Linq;

namespace GenChatBotExperiment
{
    // Technically, this should be two classes - one for input, one for parsing a blob of HTML.
    // Not needed right now, but eventually, this could have a few interfaces extracted.
    class GroupChat
    {
        private const string MESSAGES_DIV_SELECTOR = "[aria-label=Messages]";
        private const string INPUT_SPAN_SELECTOR = "[data-text=true]";
        private const string NAME_LIST_CLASS = "._29hl";
        private const string NAME_ELEMENT_CLASS = "_364g";
        private const string COLOR_BUTTON_SELECTOR = "[data-intl-translation=\"Change Color\"]";
        private const string COLOR_BUTTON_CLASS = "._5dr3";

        private IWebDriver driver;
        private Random random;

        public GroupChat(IWebDriver driver)
        {
            this.driver = driver;
            this.random = new Random();
        }

        /** Returns the list of people present in this group chat, sorted alphabetically. */
        public IList<string> ReadNames()
        {
            IWebElement namesElement = null;

            try
            {
                namesElement = driver.FindElement(By.CssSelector(NAME_LIST_CLASS));
            }
            catch (NoSuchElementException)
            {
                // This either means we switched chats during a read, or we aren't in a group chat.
                return new List<string>();
            }

            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(namesElement.GetAttribute("innerHTML"));

            return document
                 .DocumentNode
                 .Descendants()
                 .Where(node => node.GetAttributeValue("class", string.Empty) == NAME_ELEMENT_CLASS)
                 .Select(node => node.InnerText)
                 .ToList();
        }

        /** Returns the list of currently loaded messages in this group chat. */
        public IList<Message> ReadMessages()
        {
            IList<Message> messages = new List<Message>();
            IWebElement messagesElement = null;

            try
            {
                messagesElement = driver.FindElement(By.CssSelector(MESSAGES_DIV_SELECTOR));
            }
            catch (NoSuchElementException)
            {
                // This usually means we switched chats while polling. While this isn't a typical
                // use case, it's nice to be able to do this during debugging without a hard crash.
                return messages;
            }

            HtmlDocument document = new HtmlDocument();
            document.LoadHtml(messagesElement.GetAttribute("innerHTML"));

            HtmlNode messagesRoot = document.DocumentNode.ChildNodes[2];

            IEnumerable<HtmlNode> messageGroups = messagesRoot
                .ChildNodes
                .Where(node => node.OriginalName == "div")
                .Select(node => node.FirstChild);

            foreach (HtmlNode group in messageGroups)
            {
                string name = string.Empty;
                string nickname = string.Empty;
                string time = string.Empty;

                if (group.ChildNodes.Count == 1)
                {
                    name = "Noah Zimmt";
                }
                else if (group.ChildNodes.Count == 2)
                {
                    HtmlNode header = group.FirstChild;
                    HtmlNode image = header.FirstChild;
                    while (image.OriginalName != "img")
                    {
                        image = image.FirstChild;
                    }
                    name = image.Attributes["alt"].Value;
                }
                else
                {
                    // Nickname changes, color changes, etc.
                    continue;
                }

                HtmlNode content = group.LastChild;
                HtmlNode label = content
                    .FirstChild // Header tag.
                    .FirstChild;
                nickname = label.InnerText;

                IEnumerable<HtmlNode> lines = content
                    .ChildNodes
                    .Where(node => node.OriginalName == "div")
                    .Select(node => node.FirstChild);

                int lineNumber = 0;
                foreach (HtmlNode line in lines)
                {
                    // Some content does not have a time tooltip.
                    string timestring = line.GetAttributeValue("data-tooltip-content", string.Empty);
                    string textstring = line
                        .FirstChild // Div tag.
                        .FirstChild // Span tag.
                        .InnerText;
                    IEnumerable<string> emojis = line
                        .Descendants()
                        .Where(node => node.OriginalName == "img")
                        .Where(node => node.Attributes["src"].Value.StartsWith("https://static.xx.fbcdn.net/images/emoji.php/"))
                        .Select(node => node.Attributes["src"].Value);

                    Message message = new Message(
                        DateTime.UtcNow.Date, name, nickname, timestring, lineNumber, textstring, emojis);
                    messages.Add(message);

                    // Debug
                    Console.WriteLine(JObject.FromObject(message));
                }
            }

            return messages;
        }

        /** Writes a message in the input field of the chat window. */
        public void WriteMessage(string message)
        {
            IWebElement textbox = driver.FindElement(By.CssSelector(INPUT_SPAN_SELECTOR));

            Actions actions = new Actions(driver);
            actions.MoveToElement(textbox);
            actions.Click();
            actions.SendKeys(message);
            actions.Build().Perform();
        }

        /** Sends the message currently in the input field of the chat window. */
        public void SendMessage()
        {
            WriteMessage(Keys.Enter);
        }

        /** Sets the chat color to a random color. */
        public String SetRandomChatColor()
        {
            IWebElement colorButton = driver.FindElement(By.CssSelector(COLOR_BUTTON_SELECTOR));

            Actions actions = new Actions(driver);
            actions.MoveToElement(colorButton);
            actions.Click();
            actions.Build().Perform();

            IList<IWebElement> colorOptions = driver.FindElements(By.CssSelector(COLOR_BUTTON_CLASS)).ToList();

            IWebElement color = colorOptions[random.Next() % colorOptions.Count];
            actions.MoveToElement(color);
            actions.Click();
            actions.Build().Perform();

            return color.GetAttribute("aria-label");
        }

    }
}
